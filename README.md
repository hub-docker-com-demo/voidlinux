# voidlinux

XBPS, runit, glibc, musl and rolling release https://voidlinux.org/

* https://voidlinux.org/packages/
* https://hub.docker.com/r/voidlinux/voidlinux
* https://hub.docker.com/r/voidlinux/voidlinux-musl

# XBPS
## Features
### "(update) breaks installed pkg"
* https://github.com/void-linux/void-packages/issues/17220
